-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 18, 2022 lúc 10:48 AM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `sonnt_db`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `image`, `created`, `modified`) VALUES
(22, '123@gmail.com', '123', '$2y$10$TSor0.kvIpN1YzP3LQQn0uqbLAwEkeQ1XQGdhuyeGPgBMuWszuLLa', 'sodovatly.png', '2022-08-16 08:34:11', '2022-08-16 10:11:09'),
(23, 'nhocmaccute@gmail.com', 'nhocmac', '$2y$10$lJh0MMARJucm5AHbU.32dOUV0MSd5A9tXmdzdyBgNoeNpg6Klcjyy', 'nhocmac.jpg', '2022-08-16 09:46:23', '2022-08-16 10:10:10'),
(24, 'hathien@gmail.com', 'hathien01', '$2y$10$6ujRQkKqujvGasy/6HutiOEx3gwXWIxVrG2qUkLkV2VcOoDNNXJbO', 'nhattieukhuynhthanh.png', '2022-08-16 09:53:13', '2022-08-16 10:01:12'),
(25, 'chinhhi@gmail.com', 'chinhhi', '$2y$10$f0z3/ULP0MxQNv64F3u/7eMgbGKNivyMNQUbl.idjM41qAlEeg0hu', 'avt01.png', '2022-08-16 09:58:10', '2022-08-16 09:58:10'),
(26, '0011@gmail.com', '0011', '$2y$10$nKG8ZGcKd6vBUDEw30RQeud7orhtorPTXhk7UotoSOBRxky4G2nQi', '03.jpg', '2022-08-17 02:07:03', '2022-08-18 03:55:18'),
(27, '0012@gmail.com', '0012', '$2y$10$KnnETgRomwzV5Ak1MDbQ4Ob5oeVy4MucyKv30Xz40BA65YYP1qmgi', '0002.PNG', '2022-08-17 03:42:57', '2022-08-17 03:42:57'),
(28, '0013@gmail.com', '0013', '$2y$10$m6SSRgcdv4IgKRk8by2bp.qiFYTmIidnLi5dJlXJmUoSuzk1BaJEy', '03.jpg', '2022-08-17 03:47:40', '2022-08-17 04:08:21'),
(29, '0014@gmail.com', '0014', '$2y$10$xdpDhW80VcmsbDehKLOAmuOyjQyCUDaUUY/765qZKHuC5m8Axk3zO', '04.jpg', '2022-08-17 03:50:10', '2022-08-17 04:08:34'),
(30, 'tienpk@gmail.com', 'tienpk', '$2y$10$Hqoik3AM4vY0cOJSsivkXOJFQ5ccLjBBF2tM8JWFeEVqZG8LLzPuW', '03.jpg', '2022-08-17 03:51:15', '2022-08-17 03:51:15'),
(31, '0015@gmail.com', '0015', '$2y$10$EoD6Si5td396WnpETri3cO00QOOhHebd2rFW0YYKZc3FswDarIQNi', '06.jpg', '2022-08-17 03:55:00', '2022-08-17 03:55:00'),
(32, '123456@gmai.com', 'tienpk1234', '$2y$10$Ot.ftR.gCojutAeIHa20EOIWCx7D/Tb2BjfVgF4YAZbyYF7xY3RQm', '06.jpg', '2022-08-17 03:59:01', '2022-08-17 03:59:01'),
(33, '3@gmail.com', '123456abc', '$2y$10$jDUyRhjmdyILmDl4opIdVOgjAseTEzoBnSBvwtgCmkEZL5uhN2Pby', '06.jpg', '2022-08-17 04:00:21', '2022-08-17 04:00:21'),
(34, '0016@gmail.com', '0016', '$2y$10$8XBqOFWS8hjaxEFHzbRSwe.68AE5pHrGjJoydxIsblBAYKbBUFl7i', '06.jpg', '2022-08-17 04:02:42', '2022-08-17 04:02:42'),
(35, '0017@gmail.com', '0017', '$2y$10$xadKf2jrDZZ8yVj9ZPFu7udjQyAJEhOygwVkie5kIR7UJZKV332zu', '04.jpg', '2022-08-17 04:07:01', '2022-08-17 04:07:01'),
(36, '0018@gmail.com', '0018', '$2y$10$Z2w6Y6KfVDvQb2BMU2IbXuCcEagdmyqw2FUKr4jyUNKwX/a6OsMDS', '01.jpg', '2022-08-17 04:07:40', '2022-08-17 04:07:40'),
(37, '001@gmail.com', '001', '$2y$10$WJmg1VISWfH8.lY6srlSl.prFyS180RU/AcZFTYWcx76fc.WL/iHm', '05.jpg', '2022-08-18 01:56:53', '2022-08-18 01:56:53'),
(38, '002@gmail.com', '002', '$2y$10$.FQBNpgEk3cr6NXy1DcQ0eKKlCamj2GNPxe.K/u29xPe1VILrIM.m', '04.jpg', '2022-08-18 01:58:06', '2022-08-18 01:58:06'),
(39, '004@gmail.com', 'kiennhat01', '$2y$10$jXhIk4Qiq3lcho5D2MthX.amn1ZGYmpr.ps3JH4n1Siv0DHm7rWeC', '04.jpg', '2022-08-18 03:15:57', '2022-08-18 03:15:57'),
(40, '025@gmail.com', '025', '$2y$10$ZUAbmqBgmpC4XYa6clsmOuNICTWMr4GOtRD.m70tpCm4bHAhJLcvu', '296920545_1521715701654643_6732876280749182371_n.jpg', '2022-08-18 03:59:57', '2022-08-18 04:18:31'),
(41, '026@gmail.com', '026', '$2y$10$Icz12POgKobOvdI0oXND6OdZvItVkAeuig3sZOSyqQ13GUunnZHVG', '296920545_1521715701654643_6732876280749182371_n.jpg', '2022-08-18 04:10:45', '2022-08-18 04:18:46'),
(42, '027@gmail.com', '027', '$2y$10$4DN7ijx18MaLxg7h0KalFuvyzFjI7AO2yOjfY8fXBFRWSvtoqyy3e', '296920545_1521715701654643_6732876280749182371_n.jpg', '2022-08-18 04:19:11', '2022-08-18 04:19:36'),
(43, '031@gmail.com', '031', '$2y$10$4f7jKO6kupx4M0mHnKBn..vm1RHcKMjdwClXyUHlY02m1q0BoS3zS', '296634266_1337901546618633_2000412905152453244_n.jpg', '2022-08-18 07:06:34', '2022-08-18 07:06:34'),
(44, '032@gmail.com', '032', '$2y$10$QNAcmJD66uqq4n0h3gqgGufLlmS8uZ080m6gZCzZMvl7pXFDh/PYK', '296634266_1337901546618633_2000412905152453244_n.jpg', '2022-08-18 07:08:30', '2022-08-18 07:08:30'),
(45, '033@gmail.com', '033', '$2y$10$8B9uuaGuzcOk2N.VsxZCMe9j.3iHaDIapUIe4xRjf6aUR8rF8082e', '279427769_367530148495778_2890912080273437695_n.jpg', '2022-08-18 07:31:04', '2022-08-18 07:31:04'),
(46, '034@gmail.com', '034', '$2y$10$hZFCw7.5zLEkTqan4Raz6uZCjCb3pNXAfBk6olVLE1qRHhISnV57S', 'composition.png', '2022-08-18 08:04:15', '2022-08-18 08:04:15');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
