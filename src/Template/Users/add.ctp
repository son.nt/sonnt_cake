<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <form method="post" action="add" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_csrfToken" value="<?= $this->request->getParam('_csrfToken');?>" />
    <fieldset>
        <legend>Add User</legend>
        <label for="email">email</label>
        <input id="email" type="text" name="email" maxlength="255">
        <label for="username">username</label>
        <input id="username" type="text" name="username" maxlength="50">
        <label for="password">password</label>
        <input id="password" type="password" name="password" maxlength="255">
        <label>image</label>
        <input type="file" name="image"><br><br>
    </fieldset>
        <button type= "submit" >submit</button>
    </form>
</div>
