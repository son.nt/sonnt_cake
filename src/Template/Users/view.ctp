<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3>User</h3>
    <table class="vertical-table">
        <tr>
            <th scope="row">Email</th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row">Username</th>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <th scope="row">Password</th>
            <td>***********************</td>
        </tr>
        <tr>
            <th scope="row">Image</th>
            <td><?= $this->Html->image($user->image); ?></td>
        </tr>
        <tr>
            <th scope="row">Created</th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row">Modified</th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
</div>
