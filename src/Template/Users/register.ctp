<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="users form large-9 medium-8 columns content">
    <form method="post" action="register" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_csrfToken" value="<?= $this->request->getParam('_csrfToken');?>" />
        <fieldset>
            <legend>Register</legend>
            <label for="email">email</label>
            <input id="email" type="text" name="email" maxlength="255"><br><br>
            <input type="hidden" id="username" type="text" name="username" maxlength="50">
            <label for="password">password</label>
            <input id="password" type="password" name="password" maxlength="255"><br><br>
            <input type="hidden" type="file" name="image"><br><br>
        </fieldset>
        <button type= "submit" >submit</button>
    </form>
</div>
