<?php
namespace App\Controller;

use App\Model\Table\UsersTable;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * Users Controller
 *
 * @property UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['register','login']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $key = $this->request->getQuery('key');

        if($key){
            $query = $this->Users->find('all')->where(['email like'=>'%'.$key.'%']);
        }else{
            $query = $this->Users;
        }
        $users = $this->paginate($query);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();

        if ($this->request->is('post')) {
            $data= $this->request->getData();

            if (!empty($this->request->getData('image')['name'])) {
                $filename1 = $data['image']['name'];
                $ext = pathinfo($filename1, PATHINFO_EXTENSION);

                if(!in_array($ext, ['png', 'jpg', 'jpeg'])){
                    $this->Flash->error(__('Only images smaller than 2M'));
                    $this->redirect(['action' => 'add']);
                }
                else {
                    $filename = time().'.'.$ext;
                    $uploadPath = WWW_ROOT . 'img/';
                    $uploadFile = $uploadPath . $filename;

                    if (move_uploaded_file($data['image']['tmp_name'], $uploadFile)) {
                        $data['image'] = $filename;
                    }
                    $user = $this->Users->patchEntity($user, $data);
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__('The user has been saved.'));

                        return $this->redirect(['action' => 'index']);
                    }
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));
                }
        }
        }
        $this->set(compact('user'));}

    /**
     * search method
     */
    public function search()
    {
        $this->request->allowMethod('ajax');

        $keyword = $this->request->query('keyword');

        $query = $this->Tags->find('all',[
            'conditions' => ['name LIKE'=>'%'.$keyword.'%'],
            'order' => ['Tags.id'=>'DESC'],
            'limit' => 10
        ]);

        $this->set('tags', $this->paginate($query));
        $this->set('_serialize', ['tags']);
    }

    /**
     * Edit method
     *
     * @param int|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit( $id = null )
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data= $this->request->getData();

            if (!empty($this->request->getData('image')['name'])) {
                $filename = $data['image']['name'];
                $uploadPath = WWW_ROOT.'img/';
                $uploadFile = $uploadPath . $filename;

                if (move_uploaded_file($data['image']['tmp_name'], $uploadFile)) {
                    $data['image'] = $filename;
                }
            }
            $user = $this->Users->patchEntity($user, $data);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);

        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * login method
     * @return \Cake\Http\Response|void|null
     */
    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            if ($user) {
                $this->Auth->setUser($user);

                return $this->redirect(['action'=>'index']);
            }
            $this->Flash->error('Your username or password is incorrect.');
        }
    }

    /**
     * register method
     * @return \Cake\Http\Response|void|null
     */
    public function register()
    {
        $user = $this->Users->newEntity();

        if ($this->request->is('post')) {
            $data= $this->request->getData();

            if (!empty($this->request->getData('image')['name'])) {
                $filename = $data['image']['name'];
                $uploadPath = WWW_ROOT.'img/';
                $uploadFile = $uploadPath . $filename;

                if (move_uploaded_file($data['image']['tmp_name'], $uploadFile)) {
                    $data['image'] = $filename;
                }
            }
            $user = $this->Users->patchEntity($user, $data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));}


    /**
     * logout method
     * @return \Cake\Http\Response|null
     */

    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }
}
